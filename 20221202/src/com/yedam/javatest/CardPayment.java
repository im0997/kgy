package com.yedam.javatest;

public class CardPayment implements Payment {
	//필드
	double cardRatio;
	//생성자
	public CardPayment(double cardRatio) {
		this.cardRatio = cardRatio;
		System.out.println("**카드로 결제시 할인정보");
	}
	//메서드
	@Override
	public void showInfo() {
		System.out.println("온라인 결제시 총 할인율 : "+(ONLINE_PAYMENT_RATIO + this.cardRatio) );
		System.out.println("오프라인 결제시 총 할인율 : "+(OFFLINE_PAYMENT_RATIO + this.cardRatio) );
	}
	@Override
	public int online(int price) {
		return (int) (price -(ONLINE_PAYMENT_RATIO + this.cardRatio)*price);
	}
	@Override
	public int offline(int price) {
		return (int) (price-(OFFLINE_PAYMENT_RATIO + this.cardRatio)*price);
	}
	
	//

}
