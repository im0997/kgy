package com.yedam.javatest;

public class DGBCard extends Card{
	//필드
		String company;
		String cardStaff;

	public DGBCard(String cardNo, int validDate, int CVC, String cardStaff) {
		super(cardNo, validDate, CVC);
		this.company = "대구은행";
		this.cardStaff=cardStaff;
	}

	@Override
	public void showCardInfo() {
		super.showCardInfo();
//		System.out.println("카드정보 - "+cardNo+", 유효기간 : "+ validDate+", CVC : "+CVC);
		System.out.println("담당직원 - "+cardStaff +", "+ company );
	}
	
	
}
