package com.yedam.javatest;

public class Person {
	
	//필드
	String ssn;
	String name;
	String address;
	//생성자
	public Person() {
	}
	
	public Person(String ssn, String name, String address) {
		this.ssn=ssn;
		this.name=name;
		this.address=address;
	}

	public String getSsn() {
		return ssn;
	}
	public String getName() {
		return name;
	}
	public String getAddress() {
		return address;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setAddress(String address) {
		this.address = address;
	}
}
