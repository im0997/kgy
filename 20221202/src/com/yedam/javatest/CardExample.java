package com.yedam.javatest;

public class CardExample {
	public static void main(String[] args) {
		Card cd;
		cd = new Card("5432-4567-9534-3657", 20251203, 253);
		cd.showCardInfo();
		
		
		cd = new TossCard("5432-4567-9534-3657", 20251203, 253, "신빛용");
		cd.showCardInfo();
		
		cd = new DGBCard("5432-4567-9534-3657", 20251203, 253, "신빛용");
		cd.showCardInfo();
		
	}
}
