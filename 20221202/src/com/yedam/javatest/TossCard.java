package com.yedam.javatest;

public class TossCard extends Card{
	
	//필드
	String company;
	String cardStaff;
	
	//생성자
	public TossCard(String cardNo, int validDate, int CVC, String cardStaff) {
		super(cardNo, validDate, CVC);
		this.company = "Toss";
		this.cardStaff=cardStaff;
	}
	//메서드

	@Override
	public void showCardInfo() {
		System.out.println("카드정보 - Card NO,"+cardNo);
		System.out.println("담당직원 -"+cardStaff+","+company);
		
	}

	
	

}
