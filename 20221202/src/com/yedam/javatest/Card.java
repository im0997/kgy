package com.yedam.javatest;

public class Card {	
	
	//필드
	String cardNo;
	int validDate;
	int CVC;
	
	//생성자
	public Card(String cardNo,	int validDate, 	int CVC){
		this.cardNo =cardNo;
		this.validDate=validDate;
		this.CVC=CVC;
		}
	//getter
	public String getCardNo() {
		return cardNo;
	}
	public int getValidDate() {
		return validDate;
	}
	public int getCVC() {
		return CVC;
	}	
	//메서드
	public void showCardInfo() {
		System.out.println("카드정보 ( Card NO : " + this.cardNo+", 유효기간 : "+this.validDate+", CVC : "+this.CVC+")" );
	}
	
	
	
	
	
	
	
	
	
	
	
}
