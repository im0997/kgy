package com.yedam.tset11291;

public class PortableNotebook implements Notebook, Tablet   {
	
	//필드
	int mode;
	String program;
	String striming;
	String app;
	String OS;
	
	//생성자
	public PortableNotebook(String program, String striming,String app,String OS) {
		mode = NOTEBOOK_MODE;
		System.out.println("NOTEBOOK_MODE");
		this.program = program;
		this.striming = striming;
		this.app =app;
		this.OS = OS;
	}
	
	//메서드
	@Override
	public void writeDocumentaion() {
		System.out.println(this.program + "을 통해 문서를 작성.");	
	}
	@Override
	public void watchVideo() {
		System.out.println(this.striming +"를 시청.");
	}
	@Override
	public void searchInternet() {
		System.out.println(this.OS + "를 통해 인터넷을 검색.");	
	}
	@Override
	public void useApp() {
		if(mode == NOTEBOOK_MODE) {
			System.out.println(this.app+"앱을 실행.");
			mode = TABLET_MODE;
		} else {
			System.out.println(this.app+"앱을 실행.");
		}
	}
	public void changeMode() {
		if(mode == NOTEBOOK_MODE) {
			mode = TABLET_MODE;
			System.out.println("TABLET_MODE");
		} else if(mode == TABLET_MODE) {
			mode = NOTEBOOK_MODE;
			System.out.println("NOTEBOOK_MODE");
		}
	}
}
