package com.yedam.tset1130;

import java.util.Scanner;

public class test {
	public static void main(String[] args) {

		// 1) 문자열 개수 세기
//		-> 입력 문자열에서 알파벳, 숫자, 공백의 개수를 구하시오.
//		예시) 
//		-> 입력 : 1a2b3c4d 5e
//		-> 출력 : 문자 :5개, 숫자:5개, 공백 : 1개
		String str = "1a2b3c4d 5e";
		int space =0;
		int num=0;
		int cha=0;
		for(int i=0; i<str.length(); i++) {
			char tempStr = str.charAt(i);
			
			if(tempStr == ' ') {
				space++;
				
			} else if(tempStr >= '0' && tempStr<='9') {
				num++;
			} else if (tempStr >='a' && tempStr<='z') {
				cha++;
			}
			
		}System.out.print("문자 :" + cha + "개, 숫자 :"+ num+"개, 공백 :"+space +"개 \n");
		
		System.out.println("==========================================");
		
		
		
		//2) 중복이 안되는 문자열에서 두 문자사이의 거리 구하기
//		조건 : 입력되는 두 문자를 제외한 가운데 문자의 갯수를 두 문자간 거리로 한다.
//		각문자의 위치를 찾은 다음 큰수 - 작은 수
//		예시)
//		-> 입력 : "abcdefghijklmnopqrstuvwxyz"
//		-----------------------------------
//		-> 입력 : 첫번째 문자 : c
//		-> 입력 : 두번째 문자 : f
//		-> 출력 : 두 문자간의 거리 : 2
//		------------------------------------
//		-> 입력 : 첫번째 문자 : e
//		-> 입력 : 두번째 문자 : a
//		-> 출력 : 두 문자간의 거리 : 3
		Scanner sc = new Scanner(System.in);
		
		String alpha = "abcdefghijklmnopqrstuvwxyz";
		
		System.out.println("첫번째 문자 >");
		String firstWd = sc.nextLine();
		System.out.println("두번째 문자 >");
		String secondWd = sc.nextLine();
		
		int firstIndex = alpha.indexOf(firstWd); //2
		int secondIndex = alpha.indexOf(secondWd); //5
//		각 문자의 위치, 다음 큰 수 - 작은 수
		if(firstIndex < secondIndex) {
			System.out.println("두 문자간의 거리" + (secondIndex-firstIndex-1));
		} else if(firstIndex > secondIndex) {
			System.out.println("두 문자간의 거리" + (firstIndex-secondIndex-1));
		}
		
		
//		3) 중복문자 제거
//		입력 : aaabbccceedddd
//		출력 : abced
		
		//중복문자가 존재하는지에 대한 여부 체크
		String strList = "aaabbccceedddd";
		//indexOf에서 중복된 문자열이 있으면 -> 중복된 문자열 중 제일 첫 번째 값을 반환
		//str.indexOf(charAt(i))==i
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<strList.length(); i++) {
			if(strList.indexOf(strList.charAt(i))==i) {
				sb.append(strList.charAt(i));
			}
		}
		System.out.println(sb);
	}
}
