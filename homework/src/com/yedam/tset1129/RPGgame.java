package com.yedam.tset1129;

//2) RPGgame 클래스를 정의한다.
//- Keypad를 구현하는 클래스.
public class RPGgame implements Keypad {  

	
	//필드 - 현재 모드를 값으로 가지는 필드를 가지며 	
	private int nowmode; // private<???

	//생성자 
	//생성자를 이용하여 값을 초기화한후 "RPGgmae 실행"을 출력한다.
	//( 단, 매개변수를 이용하지 않으며 최초의 값은 NORMAL_MODE 이다. )
	public RPGgame() {
		nowmode = Keypad.NORMAL_MODE;
		System.out.println("RPGgmae 실행");
	}
	
	//메소드
	//- Keypad 인터페이스에 정의된 추상 메서드를 다음과 같이 오버라이딩한다.
	@Override
	public void leftUpButton() {
		System.out.println("캐릭터가 위쪽으로 이동한다");
	}
	@Override
	public void leftDownButton() {
		System.out.println("캐릭터가 아래쪽으로 이동한다");
	}
	@Override
	public void rightUpButton() {
		if(nowmode == Keypad.NORMAL_MODE) {
			System.out.println("캐릭터가 한칸단위로 점프한다.");
		} else if(nowmode == Keypad.HARD_MODE) {
			System.out.println("캐릭터가 두칸단위로 점프한다.");
		}
	}
	@Override
	public void rightDownButton() {
//		switch (nowmode) {
//		case Keypad.NORMAL_MODE:
//			System.out.println("캐릭터가 일반 공격.");
//			break;
//		case Keypad.HARD_MODE:
//			System.out.println("캐릭터가 HIT 공격.");
//			break;
//		}
		
		if(nowmode == Keypad.NORMAL_MODE) {
			System.out.println("캐릭터가 일반 공격.");
		} else if(nowmode == Keypad.HARD_MODE) {
			System.out.println("캐릭터가 HIT 공격.");
		}
	}
	//(5) : 모드를 바꾸고 현재 모드를 출력하는 기능 ( NORMAL_MODE -> HARD_MODE / HARD_MODE -> NORMAL_MODE)
	@Override
	public void changeMode() {
		if(nowmode == Keypad.NORMAL_MODE ) {
			nowmode = Keypad.HARD_MODE;
			System.out.println("현재모드 : HARD_MODE");
		} else if (nowmode == Keypad.HARD_MODE) {
			nowmode = Keypad.NORMAL_MODE;
			System.out.println("현재모드 : NORMAL_MODE" );
		}
		
	}
	

}
