package com.yedam.API;

public class ObjectExample {
	public static void main(String[] args) throws ClassNotFoundException {
		Object obj1 = null;
		Object obj2 = new Object();
		
		System.out.println(obj1);
		System.out.println(obj2);
		
		//System.out.println(obj1.toString());
		System.out.println(obj2.toString());
		
		Member member = new Member("123");
		
		member.name = "김또치";
		member.ssn = "123456-1234567";
		
		System.out.println(member.toString());
		
		
		//System 클랙스
		// 강제종료 : System.exit(0);
//		for(int i=0; i<10; i++) {
//			System.out.println(i);
//			if(i==5) {
//				System.exit(0);
//			}
//		}
		
		//현재 시각 읽기
		
		long time1 = System.nanoTime(); // 현재 시각 읽음 -1
		
		int sum =0;
		
		for(int i=0; i<=1000000; i++) {
			sum +=i;
		}
		long time2 = System.nanoTime();// 현재 시각 읽음 -2
		System.out.println(time1);
		System.out.println(time2);
		System.out.println("1~1000000까지의 합 : "+sum);
		System.out.println("소요 시간 :" + (time2 - time1)+"나노 초 소요");
		System.out.println("System.currentTimeMillis()");

		System.out.println("=========================================================");
		//Class 클래스
		
		System.out.println("Class 클래스");
		
		//첫 번째 방법 - Class 에서 정보 얻기
		
		Class clazz = Member.class;
		System.out.println("첫번째 방법 : " + clazz);
		
		//두 번째 방법 - 클래스 경로 입력해서 정보얻기
		clazz = Class.forName("com.yedam.API.Member");
		System.out.println("두번째 방법"+clazz);
		
		//세 번째 방법 - 객체를 생성하고 객체에서 클래스 정보얻기
		Member mem = new Member("예담");
		clazz=mem.getClass();
		System.out.println("세번째방법"+clazz);
		
		System.out.println(clazz.getName());
		System.out.println(clazz.getSimpleName());
		System.out.println(clazz.getPackageName());
		System.out.println(clazz.getPackage().getName());
		
		//파일 경로 읽어 오기 - 절.대.경.로
		String photoPath = clazz.getResource("말티즈.jfif").getPath();
		System.out.println(photoPath);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
				
	}
}
