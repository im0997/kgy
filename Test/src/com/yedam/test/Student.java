package com.yedam.test;

public class Student {
	//필드 //set,get < 필드가 private
	private String name;
	private String major;
	private String grade;
	private int programing;
	private int datebase;
	private int OS;
	//생성자
	//클래스를 통한 객체를 생성할때 첫번째로 수생하는 일들을 모아두는 곳.
	// 필드에 대한 데이터를 객체를 생성할때, 초기화할 예정이라면, 
	// 생성자에서 this키워드를 활용해서 필드 초기화하면 
	
	//메소드
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMajor() {
		return major;
	}
	public void setMajor(String major) {
		this.major = major;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public int getPrograming() {
		return programing;
	}
	public void setPrograming(int programing) {
		if(programing<0) {
			this.programing =0;
		}
		this.programing = programing;
	}
	public int getDatebase() {
		return datebase;
	}
	public void setDatebase(int datebase) {
		if(datebase<0) {
			this.datebase=0;
		}
		this.datebase = datebase;
	}
	public int getOS() {
		return OS;
	}
	public void setOS(int oS) {
		if(OS<0) {
			this.OS=0;
		}
		OS = oS;
	}
	
	
	//생성자	
	//메소드
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
