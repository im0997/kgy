package com.yedam.test;

public class StudentExam {
public static void main(String[] args) {
	Student std = new Student();
	
	std.setName("김또치");
	std.setGrade("2학년");
	std.setMajor("컴퓨터공학과");
	std.setPrograming(50);
	std.setDatebase(60);
	std.setOS(90);
	
	System.out.println("이름 : " + std.getName());
	System.out.println("학과 : " + std.getGrade());
	System.out.println("학년 : " + std.getMajor());
	System.out.println("프로그래밍 언어 점수 : " + std.getPrograming());
	System.out.println("데이터베이스 점수 : " + std.getDatebase());
	System.out.println("운영체제 점수 : " + std.getOS());
	
}
}
