package com.yedam.test2;

public class EmployeeExample {
	public static void main(String[] args) {
//		3) 아래와 같은 출력결과가 나오도록 실행코드를 구현한다.
//	- 출력결과
//		이름:이지나  연봉:3000  부서:교육부
//		수퍼클래스
//		서브클래스
		EmpDept emp = new EmpDept("이지나", "3000", "교육부"); // 생성자를 통한 데이터 초기화
		emp.getInformation();
		emp.print();
	}
}
