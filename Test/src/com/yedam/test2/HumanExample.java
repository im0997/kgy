package com.yedam.test2;

public class HumanExample {
	public static void main(String[] args) {
		Human human = new StandardWeightInfo("홍길동", 168,45);		
		human.getInformation();
		
		System.out.println( "\n");
		
		human = new ObesityInfo("박둘이" , 168 , 90); // 자동 타입변환
		human.getInformation();
	}
}
