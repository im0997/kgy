package com.yedam.inter4; //8-2(인터페이스 상속) p.399

public class ImplementC implements InterfaceC {

	@Override
	public void methodA() {
		System.out.println("ImpleC-methodA 실행");
	}

	@Override
	public void methodB() {
		System.out.println("ImpleC-methodB 실행");
	}

	@Override
	public void methodC() {
		System.out.println("ImpleC-methodC 실행");
	}

}
