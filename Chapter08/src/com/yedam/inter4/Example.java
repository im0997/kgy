package com.yedam.inter4; //8-2(인터페이스 상속) p.399

public class Example {
	public static void main(String[] args) {
		ImplementC impl = new ImplementC();
		
		InterfaceA ia = impl;
		ia.methodA();
		
		System.out.println();
		
		InterfaceB ib = impl;
		ib.methodB();
		
		System.out.println();
		
		InterfaceC ic = impl;
		ic.methodA();
		ic.methodB();
		ic.methodC();
		
	}
}
