package com.yedam.inter; //8(2)

public interface Animal {
	
	void walk();
	void fly();
	void sing();

}
