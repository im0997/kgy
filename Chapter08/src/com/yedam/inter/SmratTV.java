package com.yedam.inter; //8(4)다중인터페이스

public class SmratTV implements RemoteControl, Searchable{
	
	private int volume;
	//Searchable

	@Override
	public void search(String url) {
		// TODO Auto-generated method stub
		System.out.println(url + "을 검색합니다.");
	}
	
	//ReomteContr
	@Override
	public void turnOn() {
		System.out.println("TV를 켭니다.");
	}

	@Override
	public void turnOff() {
		System.out.println("TV를 끕니다.");
	}

	@Override
	public void setVolume(int volume) {
		//최대 소리 이상로 데이터가 들어올 때
		if(volume > RemoteControl.MAX_VOLUME) {
			this.volume = RemoteControl.MAX_VOLUME;
		}
		//최소 소리 이하로 데이터가 들어올 때
		else if(volume <RemoteControl.MIN_VOLUME) {
			this.volume = RemoteControl.MIN_VOLUME;
		} else {
			this.volume = volume;
		}
		System.out.println("현재 볼륨 : " + volume);
	}


	
	

}
