package com.yedam.inter; //8(3)

public class circle implements getInfo{
	//필드
	int r;
	double pi = Math.PI;
	
	
	//생성자
	public circle(int r) {
		this.r =r;
	}
	//메소드
	@Override
	public void area() {
		System.out.println("넓이를 구함 : " +(int)(Math.pow(r,2)*pi));
	}

	@Override
	public void round() {
		System.out.println("둘레를 구함 : " + (int)(2*r*pi));
	}

}
