package com.yedam.poly;

public class DrawExample {
	public static void main(String[] args) {
		//자동 타입변환
		//부모타입 변수 = new 자식 클래스()
		
		// 다형성 구현.
		// 1. 부모자식 관계가 존재해야함.
		// 2. 오버라이딩 존재.
		
		Draw figure = new Circle();
		figure.x = 1;
		figure.y = 2;
		
		figure.draw(); //오버라이딩 되어있음.
		
		figure = new Rectangle(); // 하나의 변수로 여려명의 자식 클래스를 변화 == 다형성
		
		figure.draw();
		
		
	}
}
