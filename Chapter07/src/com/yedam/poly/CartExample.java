package com.yedam.poly;

public class CartExample {
	public static void main(String[] args) {
		Car car = new Car();
		
		for(int i=0; i<=5; i++) {
			int problemLoc = car.run();
			
			switch (problemLoc) {
			//front
			
			case 1:
				System.out.println("앞 왼쪽 HanKookTire 교환");
				//Tire = 부모클래스(슈퍼클래스)
				//HanKooktire = 자식클래스(서브클래스_
				//Tire frontLeftTire = new HanKookTire("앞왼쪽",15);
				car.frontLeftTire = new HanKookTire("앞 왼쪽", 15);
				break;

			case 2:
				System.out.println("앞 오른쪽 KumhoTire 교환");
				car.frontRightTire = new KumhoTire("앞 오른쪽" ,15);
				break;
				
			case 3:
				System.out.println("뒤 왼쪽 HanKookTire 교환");
				car.frontRightTire = new HanKookTire("뒤 왼쪽" ,15);
				break;

			case 4:
				System.out.println("뒤 오른쪽 KumhoTire 교환");
				car.frontRightTire = new KumhoTire("뒤 오른쪽" ,15);
				break;
			}
			System.out.println("===================================");
		}
		
	}

}
