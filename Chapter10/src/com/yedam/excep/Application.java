package com.yedam.excep;

public class Application {
	public static void main(String[] args) {
//		try {
//			//예외가 발생할만한 코드
//		}catch() {
//			//예외가 발생 후 처리하는 코드
//		}
		//1) 자바가 먼저 컴파일 후 인식해서 예외처리.
		try {
			String str ="123";
			System.out.println(Integer.parseInt(str));
			
			double avg =1/0;
			System.out.println(avg);
			
			String str1 ="자바";
			//NumberFormatException 예외발생지점
			Integer.parseInt(str1);
			System.out.println("변환완료!!");
			
			//ClassNotFoundException 예외발생지점
			Class clazz = Class.forName("java.lang.String2");
			
			System.out.println("예외처리 발생!!");
			
		
		
		
		} 
		catch (ClassNotFoundException e) {
			e.printStackTrace(); // 예외가 발생한 경로를 추적하고 console 출력하세요.
			System.out.println("ClassNotFoundException 예외 발생");
		} catch (NumberFormatException e) {
			e.printStackTrace();
			System.out.println("NumberFormatException 예외 발생");
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception 발생!!");
		} finally {
			System.out.println("finally 항상 실행");
		}
		System.out.println("try -catch 탈출");
		
	}
}
