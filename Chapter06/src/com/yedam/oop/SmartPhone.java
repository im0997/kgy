package com.yedam.oop;

public class SmartPhone {
	
	
	//필드 객체의 정보를 저장
	String name;
	String maker;
	int price;
	//생성자 (클래스 이름과 똑같이 부여해서 만듬)
	//자바에서 생성자가 클래스 내부에 하나도 없을때 알아서 기본 생성자를 만들고 객체생성.
//	public SmartPhone() {
//		
//	}
	
	
	//this > 내 자신 (SmartPhone)
	public SmartPhone() {
		this.name ="iphone14pro";
	}
	
	public SmartPhone(String name) {
		//객체를 만들때 내가 원하는 행동 또는 데이터 저장 등등
		// 할때 여기에 내용을 구현
	}
	public SmartPhone(int price) {
		
	}	
	public SmartPhone(String name, int price) {
		this.name = name;
		this.price = price;
	}
	
	
	
	
	
	
	
	
	
	
	public SmartPhone(String name, String maker, int price) {
		this.name =name;
		this.maker =maker;
		this.price =price;
		
	}
	
	
	
	//메소드 객체의 기능을 정의
	void call() {
		System.out.println(name + "전화를 겁니다.");
	}
	void hangUp() {
		System.out.println(name + "전화를 끊습니다.");
	}
	
	// 1) 리턴 타입이 없는 경우. : void
	//리턴타입(void) 메소드이름(genInfo) 매개변수(int no) 
	void genInfo(int no) {
		System.out.println("매개 변수 : " + no);
	}
	
	// 2) 리턴 타입이 있는 경우 
	// 1. 기본타입 : int, double, long ,,,
	// 2. 참조타입 : String, 배열, 클래스 ,,,
	// 2-1) 리턴 타입이 기본타입일 경우
	int getInfo(String temp) {
		
		return 0; //리턴 타입이니까 리턴 넣어줘야함. 0==int
	}
	
	// 2-2) 리턴 타입이 참조 타입일 경우
	String[] getInfo(String[] temp) {
		
		return temp;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
