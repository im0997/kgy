package com.yedam.oop;

public class appl2 {
	public static void main(String[] args) {
		Student stu = new Student ("고길동", "예담고등학교" , 221124);
		Student stu2 = new Student ("김둘리", "예담고등학교" , 221125);
		Student stu3= new Student ("김또치", "예담고등학교" , 221126);
		
		stu.getInfo();
		stu2.getInfo();
		stu3.getInfo();
		
		Student stu4 = new Student();
		stu4.name = "홍길동";
		stu4.number = 221124;
		stu4.school = "예담고등학교";
		System.out.println("학생의이름은?"+stu4.name);
		System.out.println(stu4.number);
		System.out.println(stu4.school);
		
		Student stu5 = new Student();
		stu5.name = "김둘리";
		stu5.number=221125;
		stu5.school="예담고등학교";
		System.out.println(stu5.name);
		System.out.println(stu5.number);
		System.out.println(stu5.school);
		
		Student stu6 = new Student();
		stu6.name = "김또치";
		stu6.number=221126;
		stu6.school="예담고등학교";
		System.out.println(stu6.name);
		System.out.println(stu6.number);
		System.out.println(stu6.school);
		
	}
}
